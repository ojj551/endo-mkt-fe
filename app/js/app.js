'use strict';

var app = angular.module('nectech-app', [
	'ngCookies',

	'ui.router',
	'ui.bootstrap',

	'oc.lazyLoad',
	'ngSanitize',

	'nectech.directives',
	'nectech.factory',
	'nectech.services',
	'nectech.constant',
	// Added in v1.3
	'FBAngular',
	'ngAnimate',
	'cp.ngConfirm',
	'dtrw.bcrypt',
	'localytics.directives',
	'htmlToPdfSave',
	'moment-picker',
	'colorpicker.module',
	'datatables',
	'datatables.bootstrap',
	'angularjs-datetime-picker',
	'frapontillo.bootstrap-switch',
]);

app.run(function()
{
	// Page Loading Overlay
	public_vars.$pageLoadingOverlay = jQuery('.page-loading-overlay');

	jQuery(window).load(function()
	{
		public_vars.$pageLoadingOverlay.addClass('loaded');
	})
});


app.config(function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, ASSETS){

	$urlRouterProvider.otherwise('/login');

	$stateProvider.
		// Main Layout Structure
		state('app', {
			abstract: true,
			url: '/app',
			templateUrl: appHelper.templatePath('layout/app-body'),
			controller: function($rootScope,$scope,$location,$interval,$service,urlService,$funciones){
				PNotify.defaults.styling = 'bootstrap4';
				PNotify.defaults.icons = 'fontawesome4';
				
				var hours = 24;
				var now = new Date().getTime();
				var setupTime = localStorage.getItem('setupTime');
				if (setupTime == null) {
					localStorage.clear();
				} 
				else if(setupTime != "all")
				{
					if(now-setupTime > hours*60*60*1000) {
						localStorage.clear();
					}
				}
				$rootScope.nctechInfo = JSON.parse(localStorage.getItem("nctechInfo"));
				$rootScope.permisosSystem = JSON.parse(localStorage.getItem("permisos"));

				if(!$rootScope.nctechInfo){
					$location.path("/login");
				}
				
				$rootScope.serviceError = function(dataError)
				{
					new PNotify.error({
						title: 'Upss!',
						text: dataError.msg,
						type: 'error',
						styling:'bootstrap4',
						icons:'fontawesome4',
						hide:true,
						delay:3000,
					});
				}
				
				$rootScope.goToPath = function(url)
				{
					$location.path(url);
				}
				
				$rootScope.logoutSystem = function()
				{
					localStorage.clear();
					$location.path("/login");
				}
				
				$scope.getNotification = function()
				{
					let parametrosNotification = {idUser:$rootScope.nctechInfo.idUser};
					console.log(parametrosNotification);
					$service.POST(urlService.getNotification,parametrosNotification,$scope.showNotificationSuccess,$rootScope.serviceError);
				}
				
				$rootScope.notificaciones=[];
				
				$scope.showNotificationSuccess = function(dataNotification)
				{
					let notifications = dataNotification.data;
					
					$.each(notifications,function(index,value){
						let exist = ($funciones.objectFindByKey($rootScope.notificaciones,"idNotification",value.idNotification))?true:false;
						if(!exist)
						{
							$rootScope.notificaciones.push(value);
						}
					});
					
					if(!$rootScope.$$phase)$rootScope.$apply();
					
				}
				
				$rootScope.checkPermiso = function(idFunction)
				{
					return ($rootScope.permisosSystem.indexOf(idFunction) > -1);
				}
				
				$interval(function() {
					$scope.getNotification();
				}, 60000);
				
				$rootScope.showFooter = true;
				$rootScope.showHeader = true;
			},
			resolve:{
				notify: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.notify.pnotify
					]);
				},
			}
		}).
		
		//LOGIN
		
		state('login', {
			url: '/login',
			templateUrl: appHelper.templatePath('view/login'),
			controller: 'LoginCtrl',
			resolve: {
				notify: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.notify.pnotify
					]);
				},
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate,
						ASSETS.extra.toastr,
					]);
				},
				aes: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.encrypt.aesUtil
					]);
				},
				notifyIncludes: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.notify.pnotifyIncludes
					]);
				}
			}
		}).
		
		//HOME
		state('app.home', {
			url: '/home',
			templateUrl: appHelper.templatePath('view/home'),
			controller: 'HomeCtrl',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate,
						ASSETS.extra.toastr,
					]);
				},
				aes: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.encrypt.aesUtil
					]);
				},
				notify: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.notify.pnotify
					]);
				}
			}
		}).
		
		//GALERIA
		state('app.galeria', {
			url: '/galeria',
			templateUrl: appHelper.templatePath('view/galeria'),
			controller: 'GalleryCtrl',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate,
						ASSETS.extra.toastr,
					]);
				},
				aes: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.encrypt.aesUtil
					]);
				},
				notify: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.notify.pnotify
					]);
				},
				gallery: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.flipster
					]);
				}
			}
		}).
		
		//EVENTOS
		state('app.eventos', {
			url: '/eventos',
			templateUrl: appHelper.templatePath('view/eventos'),
			controller: 'EventosCtrl',
			resolve: {
				notify: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.notify.pnotify
					]);
				},
				timepicki:function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.timepicki
					]);
				},
				language:function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.languageDatePicker
					]);
				},
				autosize:function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.autosize
					]);
				}
			}
		}).

		//NEWSLETTER
		state('app.newsletter', {
			url: '/newsletter',
			templateUrl: appHelper.templatePath('view/newsletter'),
			controller: 'NewsletterCtrl',
			resolve: {
				notify: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.notify.pnotify
					]);
				}
			}
		}).

		//CAMPAÑAS MKT
		state('app.campanasmkt', {
			url: '/campanasmkt',
			templateUrl: appHelper.templatePath('view/campanasmkt'),
			controller: 'CampanasmktCtrl',
			resolve: {
				notify: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.notify.pnotify
					]);
				}
			}
		}).

		//REPORTES
		state('app.reportes', {
			url: '/reportes',
			templateUrl: appHelper.templatePath('view/reportes'),
			controller: 'ReportesCtrl',
			resolve: {
				notify: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.notify.pnotify
					]);
				}
			}
		}).
		
		//NEWSLETTER
		state('app.addNewsletter', {
			url: '/addNewsletter',
			templateUrl: appHelper.templatePath('view/formNewsletter'),
			controller: 'addNewsletterCtrl',
			resolve: {
				notify: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.notify.pnotify
					]);
				}
			}
		}).

		//DOCUMENTOS
		state('app.documentos', {
			url: '/documentos',
			templateUrl: appHelper.templatePath('view/documentos'),
			controller: 'DocumentosCtrl',
			resolve: {
				notify: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.notify.pnotify
					]);
				}
			}
		}).
		
		//MINUTAS
		state('app.minutas', {
			url: '/minutas',
			templateUrl: appHelper.templatePath('view/minutas'),
			controller: 'MinutasCtrl',
			resolve: {
				notify: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.notify.pnotify
					]);
				},
				confirmjs: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.confirmjs
					]);
				}
			}
		}).
		
		//MINUTAS
		state('app.addMinutas', {
			url: '/addMinutas',
			templateUrl: appHelper.templatePath('view/addMinutas'),
			controller: 'addMinutasCtrl',
			resolve: {
				datepicker:function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.datepicker
					]);
				},
				notify: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.notify.pnotify
					]);
				},
				autosize:function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.autosize
					]);
				},
				languageDatePicker:function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.languageDatePicker
					]);
				}
			}
		}).
		
		//MINUTAS
		state('app.editMinutas', {
			url: '/editMinutas',
			templateUrl: appHelper.templatePath('view/addMinutas'),
			controller: 'editMinutasCtrl',
			resolve: {
				datepicker:function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.datepicker
					]);
				},
				notify: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.notify.pnotify
					]);
				},
				autosize:function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.autosize
					]);
				},
				languageDatePicker:function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.languageDatePicker
					]);
				}
			}
		}).
		
		//VIEW MINUTAS
		state('app.viewMinutas', {
			url: '/viewMinutas',
			templateUrl: appHelper.templatePath('view/viewMinutas'),
			controller: 'viewMinutasCtrl',
			resolve: {
				
			}
		}).
		
		//PROCEDIMIENTOS
		state('app.procedimientos', {
			url: '/procedimientos',
			templateUrl: appHelper.templatePath('view/procedimientos'),
			controller: 'ProcedimientosCtrl',
			resolve: {
				notify: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.notify.pnotify
					]);
				},
				confirmjs: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.confirmjs
					]);
				}
			}
		}).
		
		//AGREGAR PROCEDIMIENTOS
		state('app.addProcedimientos', {
			url: '/addProcedimientos',
			templateUrl: appHelper.templatePath('view/formProcedimientos'),
			controller: 'addProcedimientosCtrl',
			resolve: {
				notify: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.notify.pnotify
					]);
				},
				autosize:function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.autosize
					]);
				}
			}
		}).
		
		//EDITAR PROCEDIMIENTOS
		state('app.editProcedimientos', {
			url: '/editProcedimientos',
			templateUrl: appHelper.templatePath('view/formProcedimientos'),
			controller: 'editProcedimientosCtrl',
			resolve: {
				notify: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.notify.pnotify
					]);
				},
				language:function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.languageDatePicker
					]);
				},
				autosize:function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.autosize
					]);
				}
			}
		}).
		
		//VIEW PROCEDIMIENTOS
		state('app.viewProcedimientos', {
			url: '/viewProcedimientos',
			templateUrl: appHelper.templatePath('view/viewProcedimientos'),
			controller: 'viewProcedimientosCtrl',
			resolve: {
				
			}
		}).
		
		//POLITICAS
		state('app.politicas', {
			url: '/politicas',
			templateUrl: appHelper.templatePath('view/politicas'),
			controller: 'PoliticasCtrl',
			resolve: {
				notify: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.notify.pnotify
					]);
				},
				confirmjs: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.confirmjs
					]);
				}
			}
		}).
		
		//AGREGAR POLITICAS
		state('app.addPoliticas', {
			url: '/addPoliticas',
			templateUrl: appHelper.templatePath('view/formPoliticas'),
			controller: 'addPoliticasCtrl',
			resolve: {
				notify: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.notify.pnotify
					]);
				},
				language:function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.languageDatePicker
					]);
				},
				autosize:function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.autosize
					]);
				}
			}
		}).
		
		//EDITAR PROCEDIMIENTOS
		state('app.editPoliticas', {
			url: '/editPoliticas',
			templateUrl: appHelper.templatePath('view/formPoliticas'),
			controller: 'editPoliticasCtrl',
			resolve: {
				notify: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.notify.pnotify
					]);
				},
				timepicki:function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.timepicki
					]);
				},
				language:function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.languageDatePicker
					]);
				},
				autosize:function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.autosize
					]);
				}
			}
		}).
		
		//VIEW PROCEDIMIENTOS
		state('app.viewPoliticas', {
			url: '/viewPoliticas',
			templateUrl: appHelper.templatePath('view/viewPoliticas'),
			controller: 'viewPoliticasCtrl',
			resolve: {
				
			}
		}).

		// USUARIOS
		state('app.usuarios', {
			url: '/usuarios',
			templateUrl: appHelper.templatePath('view/usuarios'),
			controller: 'Usuarios',
			resolve:{
				confirmjs:function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.confirmjs
					]);
				}
			}
		}).
		// AGREGAR USUARIOS
		state('app.add_usuarios', {
			url: '/add_usuarios',
			templateUrl: appHelper.templatePath('view/formUsuarios'),
			controller: 'addUsuarios',
			resolve: {
				
			}
		}).
		// EDITAR USUARIOS
		state('app.edit_usuarios', {
			url: '/edit_usuarios',
			templateUrl: appHelper.templatePath('view/formUsuarios'),
			controller: 'editUsuarios',
			resolve: {
				jQueryValidate: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate,
					]);
				},
			}
		}).
		// ROLES
		state('app.roles', {
			url: '/roles',
			templateUrl: appHelper.templatePath('view/roles'),
			controller: 'Roles',
			resolve:{
				confirmjs: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.plugins.confirmjs
					]);
				}
			}
		}).
		// AGREGAR ROL
		state('app.add_rol', {
			url: '/add_rol',
			templateUrl: appHelper.templatePath('view/form_roles'),
			controller: 'addRol',
			resolve: {

			}
		}).
		// EDITAR ROL
		state('app.edit_rol', {
			url: '/edit_rol',
			templateUrl: appHelper.templatePath('view/form_roles'),
			controller: 'editRol',
			resolve: {
				jQueryValidate: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate,
					]);
				},
			}
		}).
		// NOTIFICACIONES
		state('app.notificaciones', {
			url: '/notificaciones',
			templateUrl: appHelper.templatePath('view/notificaciones'),
			controller: 'Notificaciones',
			resolve: {
				jQueryValidate: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate,
					]);
				},
			}
		}).
		// FORGETPASSWORD
		state('forgetPassword', {
			url: '/forgetPassword',
			templateUrl: appHelper.templatePath('view/forgetPassword'),
			controller: 'ForgetPassCtrl',
			resolve: {
				notify: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.notify.pnotify
					]);
				},
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate,
						ASSETS.extra.toastr,
					]);
				},
				aes: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.encrypt.aesUtil
					]);
				}
			}
		});
});


app.constant('ASSETS', {
	'core': {
		'bootstrap': appHelper.assetPath('js/bootstrap.min.js'), // Some plugins which do not support angular needs this

		'jQueryUI': [
			appHelper.assetPath('js/jquery-ui/jquery-ui.min.js'),
			appHelper.assetPath('js/jquery-ui/jquery-ui.structure.min.css'),
		],

		'moment': appHelper.assetPath('js/moment.min.js'),

		'googleMapsLoader': appHelper.assetPath('app/js/angular-google-maps/load-google-maps.js')
	},

	'charts': {

		'dxGlobalize': appHelper.assetPath('js/devexpress-web-14.1/js/globalize.min.js'),
		'dxCharts': appHelper.assetPath('js/devexpress-web-14.1/js/dx.chartjs.js'),
		'dxVMWorld': appHelper.assetPath('js/devexpress-web-14.1/js/vectormap-data/world.js'),
	},

	'xenonLib': {
		notes: appHelper.assetPath('js/xenon-notes.js'),
	},

	'maps': {

		'vectorMaps': [
			appHelper.assetPath('js/jvectormap/jquery-jvectormap-1.2.2.min.js'),
			appHelper.assetPath('js/jvectormap/regions/jquery-jvectormap-world-mill-en.js'),
			appHelper.assetPath('js/jvectormap/regions/jquery-jvectormap-it-mill-en.js'),
		],
	},

	'icons': {
		'meteocons': appHelper.assetPath('css/fonts/meteocons/css/meteocons.css'),
		'elusive': appHelper.assetPath('css/fonts/elusive/css/elusive.css'),
	},	
	'plugins':{
		'confirmjs': [
			appHelper.assetPath('plugins/confirm/jquery-confirm.min.css'),
			appHelper.assetPath('plugins/confirm/angular-confirm.min.css'),
			appHelper.assetPath('plugins/confirm/jquery-confirm.min.js')
		],
		'datepicker': [
			appHelper.assetPath('plugins/datepicker/bootstrap-datepicker.min.css'),
			appHelper.assetPath('plugins/datepicker/bootstrap-datepicker.min.js'),
		],
		'timepicki': [
			appHelper.assetPath('plugins/timepicki/timepicki.css'),
			appHelper.assetPath('plugins/timepicki/timepicki.js')
		],
		'languageDatePicker':[
			appHelper.assetPath('plugins/datepicker/bootstrap-datepicker.es.min.js'),
		],
		'autosize':[
			appHelper.assetPath('plugins/autosize/autosize.min.js'),
		],
		'flipster': [
			appHelper.assetPath('css/jquery.flipster.min.css'),
			appHelper.assetPath('js/jquery.flipster.min.js')
		]
	},

	'forms': {

		'select2': [
			appHelper.assetPath('plugins/select2/select2.css'),
			appHelper.assetPath('plugins/select2/select2-bootstrap.css'),

			appHelper.assetPath('plugins/select2/select2.min.js'),
		],

		'daterangepicker': [
			appHelper.assetPath('plugins/daterangepicker/daterangepicker-bs3.css'),
			appHelper.assetPath('plugins/daterangepicker/daterangepicker.js'),
		],

		'colorpicker': appHelper.assetPath('plugins/colorpicker/bootstrap-colorpicker.min.js'),

		'selectboxit': appHelper.assetPath('plugins/selectboxit/jquery.selectBoxIt.js'),

		'tagsinput': appHelper.assetPath('plugins/tagsinput/bootstrap-tagsinput.min.js'),

		'datepicker': appHelper.assetPath('plugins/datepicker/bootstrap-datepicker.js'),

		'timepicker': appHelper.assetPath('plugins/timepicker/bootstrap-timepicker.min.js'),

		'inputmask': appHelper.assetPath('plugins/inputmask/jquery.inputmask.bundle.js'),

		'formWizard': appHelper.assetPath('plugins/formwizard/jquery.bootstrap.wizard.min.js'),

		'jQueryValidate': appHelper.assetPath('plugins/jquery-validate/jquery.validate.min.js'),

		'dropzone': [
			appHelper.assetPath('plugins/dropzone/css/dropzone.css'),
			appHelper.assetPath('plugins/dropzone/dropzone.min.js'),
		],

		'typeahead': [
			appHelper.assetPath('plugins/typeahead.bundle.js'),
			appHelper.assetPath('plugins/handlebars.min.js'),
		],

		'multiSelect': [
			appHelper.assetPath('plugins/multiselect/css/multi-select.css'),
			appHelper.assetPath('plugins/multiselect/js/jquery.multi-select.js'),
		],

		'icheck': [
			appHelper.assetPath('plugins/icheck/skins/all.css'),
			appHelper.assetPath('plugins/icheck/icheck.min.js'),
		],

		'bootstrapWysihtml5': [
			appHelper.assetPath('plugins/wysihtml5/src/bootstrap-wysihtml5.css'),
			appHelper.assetPath('plugins/wysihtml5/wysihtml5-angular.js')
		],
	},

	'uikit': {
		'base': [
			appHelper.assetPath('js/uikit/uikit.css'),
			appHelper.assetPath('js/uikit/css/addons/uikit.almost-flat.addons.min.css'),
			appHelper.assetPath('js/uikit/js/uikit.min.js'),
		],

		'codemirror': [
			appHelper.assetPath('js/uikit/vendor/codemirror/codemirror.js'),
			appHelper.assetPath('js/uikit/vendor/codemirror/codemirror.css'),
		],

		'marked': appHelper.assetPath('js/uikit/vendor/marked.js'),
		'htmleditor': appHelper.assetPath('js/uikit/js/addons/htmleditor.min.js'),
		'nestable': appHelper.assetPath('js/uikit/js/addons/nestable.min.js'),
	},

	'extra': {
		'tocify': appHelper.assetPath('js/tocify/jquery.tocify.min.js'),

		'toastr': appHelper.assetPath('plugins/toastr/toastr.min.js'),

		'fullCalendar': [
			appHelper.assetPath('js/fullcalendar/fullcalendar.min.css'),
			appHelper.assetPath('js/fullcalendar/fullcalendar.min.js'),
		],

		'cropper': [
			appHelper.assetPath('js/cropper/cropper.min.js'),
			appHelper.assetPath('js/cropper/cropper.min.css'),
		]
	},
	'encrypt': {
		'aesUtil': [
			appHelper.assetPath('plugins/aesUtil/aes.js'),
			appHelper.assetPath('plugins/aesUtil/AesUtil.js'),
			appHelper.assetPath('plugins/aesUtil/pbkdf2.js')
		]
	},
	'notify': {
		'pnotify': [
			appHelper.assetPath('plugins/pnotify/pnotify.custom.min.css'),
			appHelper.assetPath('plugins/pnotify/PNotify.js'),
		],
		'pnotifyIncludes': [
			appHelper.assetPath('plugins/pnotify/PNotifyButtons.js')
		]
	}
});