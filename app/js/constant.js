/**
 * Constantes Sistema Web UCB
 * @author ngarcia
 * @since 24/03/2018
 * 
 * UCB
 */
angular.module('nectech.constant', [])
.constant('aesUtilDefault',{
    'passphrase'    : 'ncbk3yd3l4w36',
	'salt'			: 'cf98f72acad578549a7dad185cc9b311',
	'iv'			: 'c491081e0519858ed18eb46e3c056cf1'
})
.constant('urlService',{
    'host'				:	'http://nctech.creativo25.com/api/Services/',
	'auth'				:	'auth',
	'getUsers'			:	'users',
	'saveUsuario'		:	'saveUsuario',
	'editUsuario'		:	'editUsuario',
	'statusUsuario'		:	'statusUsuario',
	'getRoles'			:	'roles',
	'rolFunction'		:	'rolFunction',
	'saveRol'			:	'saveRol',
	'editRol'			:	'editRol',
	'statusRol'			:	'statusRol',
	'areas'				:	'areas',
	'getRoles'			:	'roles',
	'eventBusiness'		:	'eventB',
	'eventCalendar'		:	'eventC',
	'saveEventCalendar'	:	'saveCalendar',
	'saveEventBussiness':	'saveBussiness',
	'editEventCalendar'	:	'editCalendar',
	'editEventBussiness':	'editBussiness',
	'searchCalendar'	:	'searchCalendar',
	'saveMinuta'		:	'saveMinuta',
	'editMinuta'		:	'editMinuta',
	'maxMinuta'			:	'maxMinuta',
	'userActive'		:	'usersActive',
	'getMinutas'		:	'minutas',
	'minutasList'		:	'minutasList',
	'getMinutasVersion'	:	'minutasVersion',
	'getInfoMinuta'		:	'minutaInfo',
	'deleteMinutas'		:	'deleteMinuta',
	'getAsuntosMinuta'	:	'asuntoMinuta',
	'getProcedimientos'	:	'procesos',
	'getProcesosVersion':	'procesosVersion',
	'getInfoProceso'	:	'procesoInfo',
	'saveProceso'		:	'saveProceso',
	'editProceso'		:	'editProceso',
	'getPoliticas'		:	'politicas',
	'getPoliticaVersion':	'politicasVersion',
	'getInfoPolitica'	:	'politicaInfo',
	'savePolitica'		:	'savePolitica',
	'editPolitica'		:	'editPolitica',
	'checkedPolitica'	:	'checkedPolitica',
	'checkedUsersPolitica':	'userAceptPolitica',
	'saveAsunto'		:	'saveAsunto',
	'getNotification'	:	'notificationMenu',
	'notificationActive':	'notification',
	'viewNotification'	:	'viewNotification',
	'getTokenForget'	:	'getTokenForget',
	'checkTokenPass'	:	'checkTokenPass',
	'changePassForget'	:	'changePasswordForget',
})
.constant('urlFiles',{
    'host'	:	'http://nctech.creativo25.com/upload/',
    'brief'	:	'brief'
});