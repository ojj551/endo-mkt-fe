app.filter('labelStatus', function($sce){
	return function(status)
	{
		status = parseInt(status);
		switch(status)
		{
			case 1:
				return $sce.trustAsHtml("<label class='badge badge-success'>ACTIVO</label>"); break;
			
			case 2:
				return $sce.trustAsHtml("<label class='badge badge-warning'>INACTIVO</label>"); break;
		}
	}
})
app.filter('labelStatusNotify', function($sce){
	return function(status)
	{
		switch(status)
		{
			case 1:
				return $sce.trustAsHtml("<label class='label label-info'>Creado</label>"); break;
			
			case 2:
				return $sce.trustAsHtml("<label class='label label-success'>Leido</label>"); break;
		}
	}
})
.filter('formatDate', function(){
	return function(date,type)
	{
		let meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		let mesesShort = ["ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DEC"];
		let dias=["DOM", "LUN", "MAR", "MIE", "JUE", "VIE", "SAB"];
		let arrayDate = date.split("-")
		date = new Date(arrayDate[0],arrayDate[1]-1,arrayDate[2]);
		let dia = date.getDate();
		let mes = date.getMonth()+1;
		let mes2 = date.getMonth();
		let año = date.getFullYear();
		let numDia = date.getUTCDay();
		
		if(mes < 10)
			mes = "0"+mes;
		if(dia < 10)
			dia = "0"+dia;
		
		let dateResult = "";
		
		switch(type)
		{
			case 1: dateResult = meses[mes2]+" "+año; break; //Enero 2018
			case 2: dateResult = mesesShort[mes2]+" "+dia+" "+dias[numDia]; break; //Enero 13 VIE
			case 3: dateResult = dia+" "+meses[mes2]+" "+año; break; //13 ENERO 2018
		}
		
		return dateResult;
	}
})
.filter('formatTime', function ($filter) {
	return function (time, format) {
		var parts = time.split(':');
		var date = new Date(0, 0, 0, parts[0], parts[1], parts[2]);
		return $filter('date')(date, format || 'h:mm a');
	}
})
.filter('formatDateTime', function ($funciones) {
	return function (datetime, type) {
		let dateTimeActividad = datetime.split(" ");
		let fechaActividad = dateTimeActividad[0].split("-");
		let timeActividad = dateTimeActividad[1].split(":");
		
		return $funciones.formatDate(new Date(fechaActividad[0],fechaActividad[1]-1,fechaActividad[2],timeActividad[0],timeActividad[1]),type);
	}
})
.filter('urlFile', function (urlFiles) {
	return function (file, tipo) {
		let urlServerFile = urlFiles.host;
		switch(tipo)
		{
			case 1: urlServerFile += urlFiles.brief+"/"+file; break;
		}
		
		return urlServerFile;
	}
})
//Filtro para cortar nombre tienda
.filter('cortarTexto', function(){
    return function(input, limit){
      return (input.length > limit) ? input.substr(0, limit)+'...' : input;
    };
})
.filter('countArray', function(){
    return function(array){
      return array.length;
    };
})
.filter('joinArray', function (urlFiles) {
	return function (array, key) {
		var arrayResult = [];
		$.each(array,function(index,value){
			arrayResult.push(value[key]);
		});
		
		return arrayResult.join();
	}
});