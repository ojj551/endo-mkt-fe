//Written by Evan Sharp (TheSharpieOne)
//Forked and updated by Ryan Lee

/*jslint indent:2, browser:true*/
/*globals angular*/
'use strict';
angular.module.exports = [function () {
  return {
    require: 'ngModel',
    restrict: 'A',
    scope: {
      match: '='
    },
    link: function (scope, elem, attrs, ctrl) {
      scope.$watch(function () {
        return (ctrl.$pristine && angular.isUndefined(ctrl.$modelValue)) || scope.match === ctrl.$modelValue;
      }, function (currentValue) {
        ctrl.$setValidity('match', currentValue);
      });
    }
  };
}];
