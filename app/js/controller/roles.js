app.controller('Roles', ['$scope', '$rootScope','$timeout','$funciones','$location','$service','urlService','$ngConfirm','DTOptionsBuilder','DTColumnDefBuilder','$sce',function($scope, $rootScope,$timeout,$funciones,$location,$service,urlService,$ngConfirm,DTOptionsBuilder,DTColumnDefBuilder,$sce){
	
	$scope.ini = function()
	{
		$service.GET(urlService.getRoles,"",$scope.listRoles,$rootScope.serviceError);
	}
	
	$scope.reini = function()
	{
		location.reload();
	}
	
	$scope.listRoles = function(dataRoles)
	{
		console.log(dataRoles);
		$scope.roles = dataRoles.data;
		$timeout(function(){
			$scope.configDatatable();
		},1000);
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.configDatatable = function()
	{
		$scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() { return $q.when($scope.roles);})
			.withBootstrap()
			.withDisplayLength(10)
			.withOption('order', [[0, 'desc']])
			.withLanguage({
                "sInfo": "Mostrando registros del _START_ al _END_",
				"sProcessing": "Procesando...",
				"sLengthMenu": "Mostrar _MENU_ registros",
				"sZeroRecords": "No se encontraron resultados",
				"sEmptyTable": "Ningún dato disponible en esta tabla",
				"sInfoEmpty": "Mostrando registros del 0 al 0",
				"sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix": "",
				"sSearch": "Buscar:",
				"sUrl": "",
				"sInfoThousands": ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
				"sFirst": "Primero",
				"sLast": "Último",
				"sNext": "Siguiente",
				"sPrevious": "Anterior"
				},
				"oAria": {
				"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
            })
			.withDOM(	"<'col-md-12'<'row'<'col-md-6'l><'col-md-6'f>>>" +
						"t" +
						"<'col-md-12'<'row'<'col-md-5'i><'col-md-7'p>>>"
					);
		$scope.dtColumnDefs = [
			DTColumnDefBuilder.newColumnDef(0),
			DTColumnDefBuilder.newColumnDef(1),
			DTColumnDefBuilder.newColumnDef(2),
			DTColumnDefBuilder.newColumnDef(3).notSortable(),
		];
	}
	
	$scope.confirmDelete = function(index)
	{
		var dataRol = $scope.roles[index];
		$.confirm({
			title: 'Aviso!',
			content: '¿Desear Eliminar al rol '+dataRol.name+'?',
			useBootstrap:true,
			animationBounce: 2.5,
			animationSpeed: 1000,
			buttons: {
				cancel:{
					text: 'Cancelar!',
					btnClass:'btn-danger'
				},
				confirm: {
					text: 'Si, Continuar', // With spaces and symbols
					btnClass:'btn-success',
					action: function () {
						$scope.statusRol(index,3);
					}
				}
			}
		});
	}
	
	$scope.statusRol = function(index,numStatus)
	{
		let parametrosSave = {status:numStatus,idRol:$scope.roles[index].idRol};
		$service.POST(urlService.statusRol,parametrosSave,$scope.reini,$rootScope.serviceError);
	}
	
	
	$scope.sendAddRol = function()
	{
		$location.path('/app/add_rol');
	}
	
	$scope.sendEditUser = function(index)
	{
		$rootScope.editRol = $scope.roles[index];
		if (!$scope.$$phase) $scope.$apply();
		$location.path('/app/edit_rol');
	}
	
	angular.element(document).ready(function () {
		$scope.ini();
	});
}]);

app.controller('addRol', function($scope, $rootScope,$timeout,$location,$message,$service,urlService ){
	$scope.iniAdd = function()
	{
		$scope.rol = {};
		$scope.rol.rol_function = [];
		$scope.isEdit = false;
		$timeout(function(){
			$('.iswitch').bootstrapSwitch();
		},1000);
	}
	
	$scope.setActiveTab = function(index){
		$scope.activeRol = index;
	  }
	
	$scope.sendRolesList = function()
	{
		$location.path('/app/roles');
		if (!$scope.$$phase) $scope.$apply();
	}
	
	$scope.activeModule = function(modulo,idFunction)
	{
		$timeout(function(){
			result = ($scope.rol.rol_function.indexOf(idFunction) > -1);
		
			switch(modulo)
			{
				case 1: $scope.activeUser  = result; break;
				case 2: $scope.activeRoles  = result; break;
				case 3: $scope.activeMinuta  = result; break;
				case 4: $scope.activeProcedimientos  = result; break;
				case 5: $scope.activePoliticas  = result; break;
			}
			if (!$scope.$$phase) $scope.$apply();
		},100);
	}
	
	$scope.saveRol = function()
	{
		console.log($scope.rol);
		var dataSaveRol = {data:$scope.rol,idUser:$rootScope.nctechInfo.idUser};
		$service.POST(urlService.saveRol,dataSaveRol,$scope.sendRolesList,$rootScope.serviceError);
	}
	
	angular.element(document).ready(function () {
		$scope.iniAdd();
	});
});

app.controller('editRol', function($scope, $rootScope,$timeout,$location,$service,urlService,$aesUtil){
	
	$scope.iniEdit = function()
	{
		$scope.rol = $rootScope.editRol;
		$scope.rol.rol_function = [];
		$scope.isEdit = true;
		let parametros = {idRol:$scope.rol.idRol};
		$service.GET(urlService.rolFunction,parametros,$scope.listFunction,$rootScope.serviceError);
	}
	
	$scope.listFunction = function(dataFunction)
	{
		$scope.rol.rol_function = dataFunction.data;
		$timeout(function(){
			$('.iswitch').bootstrapSwitch();
		},1000);
		if (!$scope.$$phase) $scope.$apply();
	}
	
	$scope.setActiveTab = function(index){
		$scope.activeRol = index;
	}
	
	$scope.sendRolesList = function()
	{
		$location.path('/app/roles');
		if (!$scope.$$phase) $scope.$apply();
	}
	
	$scope.activeModule = function(modulo,idFunction)
	{
		$timeout(function(){
			result = ($scope.rol.rol_function.indexOf(idFunction) > -1);
		
			switch(modulo)
			{
				case 1: $scope.activeUser  = result; break;
				case 2: $scope.activeRoles  = result; break;
				case 3: $scope.activeMinuta  = result; break;
				case 4: $scope.activeProcedimientos  = result; break;
				case 5: $scope.activePoliticas  = result; break;
			}
			if (!$scope.$$phase) $scope.$apply();
		},100);
	}
	
	$scope.editRol = function()
	{
		console.log($scope.rol);
		var dataSaveRol = {data:$scope.rol,idRol:$scope.rol.idRol};
		$service.POST(urlService.editRol,dataSaveRol,$scope.sendRolesList,$rootScope.serviceError);
	}
	
	angular.element(document).ready(function () {
		$scope.iniEdit();
	});
});