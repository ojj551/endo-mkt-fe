app.controller('PoliticasCtrl', ['$scope', '$rootScope','$timeout','$funciones','$location','$service','urlService','$ngConfirm','DTOptionsBuilder','DTColumnDefBuilder',function($scope, $rootScope,$timeout,$funciones,$location,$service,urlService,$ngConfirm,DTOptionsBuilder,DTColumnDefBuilder){
	
	$scope.ini = function()
	{
		let parametrosGetPolitica = {idUser:$rootScope.nctechInfo.idUser};
		$service.GET(urlService.getPoliticas,parametrosGetPolitica,$scope.getPoliticasSuccess,$rootScope.serviceError);
	}
	
	$scope.getPoliticasSuccess = function(dataPoliticas)
	{
		console.log(dataPoliticas);
		$scope.politicas = dataPoliticas.data;
		$timeout(function(){
			$scope.configDatatable();
		},1000);
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.configDatatable = function()
	{
		$scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() { return $q.when($scope.minutas);})
			.withBootstrap()
			.withDisplayLength(10)
			.withOption('order', [[4, 'desc']])
			.withLanguage({
                "sInfo": "Mostrando registros del _START_ al _END_",
				"sProcessing": "Procesando...",
				"sLengthMenu": "Mostrar _MENU_ registros",
				"sZeroRecords": "No se encontraron resultados",
				"sEmptyTable": "Ningún dato disponible en esta tabla",
				"sInfoEmpty": "Mostrando registros del 0 al 0",
				"sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix": "",
				"sSearch": "Buscar:",
				"sUrl": "",
				"sInfoThousands": ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
				"sFirst": "Primero",
				"sLast": "Último",
				"sNext": "Siguiente",
				"sPrevious": "Anterior"
				},
				"oAria": {
				"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
            })
			.withDOM(	"<'col-md-12'<'row'<'col-md-6'l><'col-md-6'f>>>" +
						"t" +
						"<'col-md-12'<'row'<'col-md-5'i><'col-md-7'p>>>"
					);
		$scope.dtColumnDefs = [
			DTColumnDefBuilder.newColumnDef(0),
			DTColumnDefBuilder.newColumnDef(1),
			DTColumnDefBuilder.newColumnDef(2),
			DTColumnDefBuilder.newColumnDef(3),
			DTColumnDefBuilder.newColumnDef(4),
			DTColumnDefBuilder.newColumnDef(5),
			DTColumnDefBuilder.newColumnDef(6),
			DTColumnDefBuilder.newColumnDef(7).notSortable(),
		];
	}
	
	$scope.editPoliticas = function(idPolitica)
	{
		$rootScope.idPoliticaEdit = idPolitica;
		$location.path("/app/editPoliticas");
	}
	
	$scope.viewPoliticas = function(idPolitica,name,isAceptada,isModal = false)
	{
		if(isModal)
			$("#modal-versionPolitica").modal("hide");
		
		if(!isAceptada || isAceptada == "0")
		{
			$ngConfirm({
				title: 'Aviso!',
				scope: $scope,
				content: 'Hola!, Si estas leyendo este mensaje, significa que revisaras la política <strong>'+name+'</strong>, te sugerimos revises cuidadosamente cada punto. Recuerda que las políticas son reglas definidas por Dirección General que nos ayudan a tener una mayor claridad, comunicación y orden en nuestro día a día de la empresa, una vez aceptado este mensaje asentamos que tu responsabilidad de lectura y revisión de toda la política.',
				useBootstrap:true,
				animationBounce: 2.5,
				animationSpeed: 1000,
				buttons: {
					cancel:{
						text: 'Cancelar!',
						btnClass:'btn-danger'
					},
					confirm: {
						text: 'Si, Acepto', // With spaces and symbols
						btnClass:'btn-success',
						action: function () {
							$scope.aceptarPolitica(idPolitica);
						}
					}
				}
			});
		}
		else{
			$rootScope.idPoliticaView = idPolitica;
			$location.path("/app/viewPoliticas");
		}
	}
	
	$scope.confirmDelete = function(index)
	{		
		$ngConfirm({
			title: 'Aviso!',
			scope: $scope,
			content: '¿Deseas Eliminar la política <strong>{{politicas["'+index+'"].name}}</strong>?',
			useBootstrap:true,
			animationBounce: 2.5,
			animationSpeed: 1000,
			buttons: {
				cancel:{
					text: 'Cancelar!',
					btnClass:'btn-danger'
				},
				confirm: {
					text: 'Si, Continuar', // With spaces and symbols
					btnClass:'btn-success',
					action: function () {
						$scope.deletePolitica($scope.politicas[index].idPolitica);
					}
				}
			}
		});
	}
	
	$scope.deletePolitica = function(idPolitica)
	{
		let param = {idPolitica:idPolitica};
		$service.POST(urlService.deletePolitica,param,$scope.ini,$rootScope.serviceError);
	}
	
	$scope.versionPolitica = function(idParent,name)
	{
		$scope.nameVersionPolitica = name;
		let parametrosPolitica = {idUser:$rootScope.nctechInfo.idUser,parent:idParent};
		$service.GET(urlService.getPoliticaVersion,parametrosPolitica,$scope.getPoliticaVersionSuccess,$rootScope.serviceError);
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.getPoliticaVersionSuccess = function(dataPolitica)
	{
		console.log(dataPolitica);
		$scope.politicaVersion = dataPolitica.data;
		$("#modal-versionPolitica").modal("show");
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.closeVersionProceso = function()
	{
		$scope.nameVersionPolitica = [];
		$scope.nameVersionProceso = null;
		$("#modal-versionPolitica").modal("hide");
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.aceptarPolitica = function(idPolitica)
	{
		let parametrosSave = {idPolitica:idPolitica,idUser:$rootScope.nctechInfo.idUser};
		console.log(parametrosSave);
		$service.POST(urlService.checkedPolitica,parametrosSave,$scope.aceptPoliticaSuccess,$rootScope.serviceError);
	}
	
	$scope.aceptPoliticaSuccess = function(dataPolitica)
	{
		$rootScope.idPoliticaView = dataPolitica.data;
		$location.path("/app/viewPoliticas");
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.viewReadUser = function(idPolitica,name,isModal = false)
	{
		if(isModal)
			$("#modal-versionPolitica").modal("hide");
		$scope.nameUserPolitica = name;
		let parametrosConsult = {idPolitica:idPolitica};
		console.log(parametrosConsult);
		$service.POST(urlService.checkedUsersPolitica,parametrosConsult,$scope.checkedUsersPoliticaSuccess,$rootScope.serviceError);
	}
	
	$scope.checkedUsersPoliticaSuccess = function(dataUserPolitica)
	{
		$scope.userPolitica =  dataUserPolitica.data;
		$("#modal-userPolitica").modal("show");
		if(!$scope.$$phase)$scope.$apply();
	}
	
	angular.element(document).ready(function () {
		$scope.ini();
	});
}]);
app.controller('addPoliticasCtrl', ['$scope', '$rootScope','$timeout','$funciones','$location','$service','urlService',function($scope, $rootScope,$timeout,$funciones,$location,$service,urlService){
	
	$scope.ini = function()
	{		
		$scope.isEdit = false;
		$scope.politica = {};
		$scope.politica.disposiciones = [];
		$scope.politica.prohibiciones = [];
		$scope.politica.responsables = [];
		$scope.politica.user = "";
		$scope.politica.area = "";
		
		$scope.addDisposicion();
		
		$scope.addProhibicion();
				
		$scope.addResponsable();
				
		if(!$scope.$$phase)$scope.$apply();
		
		$service.GET(urlService.userActive,null,$scope.userActiveSuccess,$rootScope.serviceError);
		
		$service.GET(urlService.areas,null,$scope.areasActiveSuccess,$rootScope.serviceError);
		
		$timeout(function(){
			autosize(document.querySelectorAll('textarea'));
		},100);
		
	}
	
	$scope.userActiveSuccess = function(dataUsers)
	{
		console.log(dataUsers.data);
		$scope.users = dataUsers.data;
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.areasActiveSuccess = function(dataAreas)
	{
		console.log(dataAreas.data);
		$scope.areas = dataAreas.data;
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.addDisposicion  = function()
	{
		let dataDisposicion = {description:""};
		$scope.politica.disposiciones.push(dataDisposicion);
	}
	
	$scope.addProhibicion = function()
	{
		let dataProhibicion = {description:""};
		$scope.politica.prohibiciones.push(dataProhibicion);
	}
	
	$scope.addResponsable = function()
	{
		let dataResponsable = {user:null,area:null};
		$scope.politica.responsables.push(dataResponsable);
	}
	
	$scope.remove = function(array, index){
		array.splice(index, 1);
	}
	
	$scope.savePolitica = function()
	{
		let parametrosSave = {data:$scope.politica,idUser:$rootScope.nctechInfo.idUser};
		console.log(parametrosSave);
		$service.POST(urlService.savePolitica,parametrosSave,$scope.saveSuccess,$rootScope.serviceError);
	}
	
	$scope.saveSuccess = function(dataSave)
	{
		$location.path("/app/politicas");
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.ini();
}]);

app.controller('viewPoliticasCtrl', ['$scope', '$rootScope','$timeout','$funciones','$location','$service','urlService',function($scope, $rootScope,$timeout,$funciones,$location,$service,urlService){

	$scope.iniView = function()
	{	
		if(!$rootScope.idPoliticaView)
		{
			$location.path("/app/politicas");
			return;
		}
		$scope.politicas = {};
		let parametrosViewPolitica = {idPolitica:$rootScope.idPoliticaView,idUser:$rootScope.nctechInfo.idUser};
		$service.POST(urlService.getInfoPolitica,parametrosViewPolitica,$scope.viewPoliticaSuccess,$rootScope.serviceError);
	}
	
	$scope.viewPoliticaSuccess = function(dataPolitica)
	{
		let politica = dataPolitica.data;
		$scope.politica = politica.data;
		$scope.politica.disposiciones = politica.disposiciones;
		$scope.politica.prohibiciones = politica.prohibiciones;
		$scope.politica.responsables = politica.responsables;
		$scope.politica.isAceptada = politica.isAceptada;
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.iniView ();

}]);
app.controller('editPoliticasCtrl', ['$scope', '$rootScope','$timeout','$funciones','$location','$service','urlService',function($scope, $rootScope,$timeout,$funciones,$location,$service,urlService){
	
	$scope.ini = function()
	{	
		$scope.isEdit = true;
		$service.GET(urlService.userActive,null,$scope.userActiveSuccess,$rootScope.serviceError);
		
		$timeout(function(){
			autosize(document.querySelectorAll('textarea'));
		},100);
	}
	
	$scope.userActiveSuccess = function(dataUsers)
	{
		console.log(dataUsers.data);
		$scope.users = dataUsers.data;
		if(!$scope.$$phase)$scope.$apply();
		
		$service.GET(urlService.areas,null,$scope.areasActiveSuccess,$rootScope.serviceError);
	}
	
	$scope.areasActiveSuccess = function(dataAreas)
	{
		console.log(dataAreas.data);
		$scope.areas = dataAreas.data;
		if(!$scope.$$phase)$scope.$apply();
		console.log($rootScope.idPoliticaEdit);
		let parametrosEditPolitica = {idPolitica:$rootScope.idPoliticaEdit,idUser:$rootScope.nctechInfo.idUser};
		$service.POST(urlService.getInfoPolitica,parametrosEditPolitica,$scope.getInfoPoliticaSuccess,$rootScope.serviceError);
	}
	
	$scope.getInfoPoliticaSuccess = function(dataPolitica)
	{
		
		let politica = dataPolitica.data;
		$scope.politica = politica.data;
		$scope.politica.disposiciones = politica.disposiciones;
		$scope.politica.prohibiciones = politica.prohibiciones;
		$scope.politica.responsables = politica.responsables;
		
		$scope.politica.area = $funciones.objectFindByKey($scope.areas,"idArea",$scope.politica.idArea);
		$scope.politica.user = $funciones.objectFindByKey($scope.users,"idUser",$scope.politica.idUser);
		
		let responsables = [];
		$.each($scope.politica.responsables,function(index,value){
			responsables.push($funciones.objectFindByKey($scope.areas,"idArea",value.idArea));
		});
		
		$scope.politica.responsables = responsables;
		
		if(!$scope.$$phase)$scope.$apply();
		console.log($scope.politica);
	}
	
	$scope.addDisposicion  = function()
	{
		let dataDisposicion = {description:""};
		$scope.politica.disposiciones.push(dataDisposicion);
	}
	
	$scope.addProhibicion = function()
	{
		let dataProhibicion = {description:""};
		$scope.politica.prohibiciones.push(dataProhibicion);
	}
	
	$scope.addResponsable = function()
	{
		let dataResponsable = {user:null,area:null};
		$scope.politica.responsables.push(dataResponsable);
	}
	
	$scope.remove = function(array, index){
		array.splice(index, 1);
	}
	
	$scope.editPolitica = function()
	{
		let parametrosSave = {data:$scope.politica,idUser:$rootScope.nctechInfo.idUser};
		console.log(parametrosSave);
		$service.POST(urlService.editPolitica,parametrosSave,$scope.editSuccess,$rootScope.serviceError);
	}
	
	$scope.editSuccess = function(dataSave)
	{
		$rootScope .idPoliticaEdit = null;
		$location.path("/app/politicas");
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.ini();
}]);