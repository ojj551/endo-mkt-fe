app.controller('MinutasCtrl', ['$scope', '$rootScope','$timeout','$funciones','$location','$service','urlService','$ngConfirm','DTOptionsBuilder','DTColumnDefBuilder',function($scope, $rootScope,$timeout,$funciones,$location,$service,urlService,$ngConfirm,DTOptionsBuilder,DTColumnDefBuilder){
	
	$scope.ini = function()
	{
		let parametrosGet = {idUser:$rootScope.nctechInfo.idUser};
		$service.GET(urlService.getMinutas,parametrosGet,$scope.getMinutasSuccess,$rootScope.serviceError);
	}
	
	$scope.getMinutasSuccess = function(dataMinutas)
	{
		console.log(dataMinutas);
		$scope.minutas = dataMinutas.data;
		$timeout(function(){
			$scope.configDatatable();
		},1000);
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.configDatatable = function()
	{
		$scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() { return $q.when($scope.minutas);})
			.withBootstrap()
			.withDisplayLength(10)
			.withOption('order', [[0, 'desc']])
			.withLanguage({
                "sInfo": "Mostrando registros del _START_ al _END_",
				"sProcessing": "Procesando...",
				"sLengthMenu": "Mostrar _MENU_ registros",
				"sZeroRecords": "No se encontraron resultados",
				"sEmptyTable": "Ningún dato disponible en esta tabla",
				"sInfoEmpty": "Mostrando registros del 0 al 0",
				"sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix": "",
				"sSearch": "Buscar:",
				"sUrl": "",
				"sInfoThousands": ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
				"sFirst": "Primero",
				"sLast": "Último",
				"sNext": "Siguiente",
				"sPrevious": "Anterior"
				},
				"oAria": {
				"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
            })
			.withDOM(	"<'col-md-12'<'row'<'col-md-6'l><'col-md-6'f>>>" +
						"t" +
						"<'col-md-12'<'row'<'col-md-5'i><'col-md-7'p>>>"
					);
		$scope.dtColumnDefs = [
			DTColumnDefBuilder.newColumnDef(0),
			DTColumnDefBuilder.newColumnDef(1),
			DTColumnDefBuilder.newColumnDef(2),
			DTColumnDefBuilder.newColumnDef(3),
			DTColumnDefBuilder.newColumnDef(4),
			DTColumnDefBuilder.newColumnDef(5),
			DTColumnDefBuilder.newColumnDef(6),
			DTColumnDefBuilder.newColumnDef(7),
			DTColumnDefBuilder.newColumnDef(8).notSortable(),
		];
	}
	
	$scope.editMinuta = function(idMinuta)
	{
		$rootScope.idMinutaEdit = idMinuta;
		$location.path("/app/editMinutas");
	}
	
	$scope.viewMinuta = function(idMinuta,isModal = false)
	{
		if(isModal)
			$("#modal-versionMinuta").modal("hide");
		$rootScope.idMinutaView = idMinuta;
		$location.path("/app/viewMinutas");
	}
	
	$scope.confirmDelete = function(index)
	{		
		$ngConfirm({
			title: 'Aviso!',
			scope: $scope,
			content: '¿Deseas Eliminar la minuta <strong>{{minutas["'+index+'"].name}}</strong>?',
			useBootstrap:true,
			animationBounce: 2.5,
			animationSpeed: 1000,
			buttons: {
				cancel:{
					text: 'Cancelar!',
					btnClass:'btn-danger'
				},
				confirm: {
					text: 'Si, Continuar', // With spaces and symbols
					btnClass:'btn-success',
					action: function () {
						$scope.deleteMinuta($scope.minutas[index].idMinuta);
					}
				}
			}
		});
	}
	
	$scope.deleteMinuta = function(idMinuta)
	{
		let param = {idMinuta:idMinuta};
		$service.POST(urlService.deleteMinutas,param,$scope.ini,$rootScope.serviceError);
	}
	
	$scope.versionMinuta = function(idParent,name)
	{
		$scope.nameVersionMinuta = name;
		let parametrosMinuta = {parent:idParent};
		$service.GET(urlService.getMinutasVersion,parametrosMinuta,$scope.getMinutasVersionSuccess,$rootScope.serviceError);
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.getMinutasVersionSuccess = function(dataMinutas)
	{
		console.log(dataMinutas);
		$scope.minutasVersion = dataMinutas.data;
		$("#modal-versionMinuta").modal("show");
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.closeVersionMinuta = function()
	{
		$scope.minutasVersion = [];
		$scope.nameVersionMinuta = null;
		$("#modal-versionMinuta").modal("hide");
		if(!$scope.$$phase)$scope.$apply();
	}
	
	angular.element(document).ready(function () {
		$scope.ini();
	});
}]);
app.controller('addMinutasCtrl', ['$scope', '$rootScope','$timeout','$funciones','$location','$service','urlService','$interval',function($scope, $rootScope,$timeout,$funciones,$location,$service,urlService,$interval){
	
	$scope.iniAdd = function()
	{		
		$scope.isEdit = false;
		$scope.minuta = {};
		$scope.minuta.participantes = [];
		$scope.minuta.objetivos = [];
		$scope.minuta.actividades = [];
		$scope.minuta.organizador = "";
		$scope.minuta.organizador_ext = "";
		$scope.minuta.secretario = "";
		$scope.minuta.secretario_ext = "";
		
		$scope.orgExt = false;
		$scope.secExt = false;
		
		$scope.addParticipante();
		
		$scope.addObjetivo();
		
		$scope.addActividad();
		
		$scope.getAsuntos();
		
		$scope.minuta.fecha = $funciones.formatDate(new Date(),1);
		$scope.minuta.timeInicio = $funciones.formatDate(new Date(),6);
		$scope.minuta.timeFin = $funciones.formatDate(new Date(),6);
		$scope.minuta.fw = $funciones.weekNumber(new Date());
		console.log($scope.minuta);
		if(!$scope.$$phase)$scope.$apply();
		
		$service.GET(urlService.userActive,null,$scope.userActiveSuccess,$rootScope.serviceError);
		$service.GET(urlService.maxMinuta,null,$scope.selectMaxMinuta,$rootScope.serviceError);
		$service.GET(urlService.minutasList,null,$scope.selectAsuntoMinuta,$rootScope.serviceError);
		
		$timeout(function(){
			autosize(document.querySelectorAll('textarea'));
		},100);
		
		$interval(function() {
			$scope.minuta.timeFin = $funciones.formatDate(new Date(),6);
			if(!$scope.$$phase)$scope.$apply();
		}, 1000);
	}
	
	$scope.selectMaxMinuta = function(maxMinuta)
	{
		$scope.minuta.minutaId = maxMinuta.data;
	}
	
	$scope.selectAsuntoMinuta = function(listMinuta)
	{
		$scope.minutasList = listMinuta.data;
	}
	
	$scope.userActiveSuccess = function(dataUsers)
	{
		console.log(dataUsers.data);
		$scope.users = dataUsers.data;
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.selectPuesto = function(index)
	{
		let puesto = "";
		let user = $scope.minuta.participantes[index].user;
		$.each($scope.users,function(index,value){
			if(value.idUser == user.idUser)
			{
				puesto = value.puesto;
				
				return false;
			}
		});
		$scope.minuta.participantes[index].puesto = puesto;
		
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.addParticipante = function()
	{
		let dataParticipante = {user:null,puesto:"",asistencia:false};
		$scope.minuta.participantes.push(dataParticipante);
		$timeout(function(){
			$('.toggle-participantes').bootstrapToggle({
				onstyle:"success",
				offstyle:"danger",
			});
		},100);
	}
	
	$scope.addObjetivo = function()
	{
		let dataObjetivo = {descripcion:"",fecha:$funciones.formatDate(new Date(),1)};
		$scope.minuta.objetivos.push(dataObjetivo);
		$timeout(function(){
			autosize(document.querySelectorAll('textarea'));
		},100);
	}
	
	$scope.addActividad = function()
	{
		let dataActividad = {responsable:"",name:"",asunto:"",descripcion:"",coresponsable:[],fecha:$funciones.formatDate(new Date(),8)};
		$scope.minuta.actividades.push(dataActividad);
		$timeout(function(){
			autosize(document.querySelectorAll('textarea'));
		},100);
	}
	
	$scope.remove = function(array, index){
		array.splice(index, 1);
	}
	
	$scope.getWeekNumer = function()
	{
		let fechaMinuta = $scope.minuta.fecha.split("-");
		console.log(new Date(fechaMinuta[2],fechaMinuta[1],fechaMinuta[0]))
		$scope.minuta.fw = $funciones.weekNumber(new Date(fechaMinuta[2],fechaMinuta[1]-1,fechaMinuta[0]));
	}
	
	$scope.saveMinuta = function()
	{
		let parametrosSave = {data:$scope.minuta,idUser:$rootScope.nctechInfo.idUser};
		console.log(parametrosSave);
		$service.POST(urlService.saveMinuta,parametrosSave,$scope.saveSuccess,$rootScope.serviceError);
	}
	
	$scope.saveSuccess = function(dataSave)
	{
		$location.path("/app/minutas");
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.getAsuntos = function()
	{
		$service.GET(urlService.getAsuntosMinuta,null,$scope.getAsuntosMinutaSuccess,$rootScope.serviceError);
	}
	
	$scope.getAsuntosMinutaSuccess = function(dataAsuntos)
	{
		$scope.asuntos = dataAsuntos.data;
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.addAsunto = function()
	{
		$("#modal-addAsunto").modal("show");
	}
	
	$scope.saveAsunto = function()
	{
		let parametrosSave = {data:$scope.asunto};
		console.log(parametrosSave);
		$service.POST(urlService.saveAsunto,parametrosSave,$scope.saveAsuntoSuccess,$rootScope.serviceError);
	}
	
	$scope.saveAsuntoSuccess = function(dataAsunto)
	{
		new PNotify.success({
			title: 'Upss!',
			text: dataAsunto.msg,
			type: 'success',
			styling:'bootstrap4',
			icons:'fontawesome4',
			hide:true,
			delay:3000,
		});
		$("#modal-addAsunto").modal("hide");
		$scope.getAsuntos();
	}
	
	$scope.setOrgnizador = function(type)
	{
		$scope.minuta.organizador = "";
		$scope.minuta.organizador_ext = "";
		if(type == 2)
			$scope.orgExt = true
		if(type == 1)
			$scope.orgExt = false;
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.setSecretario = function(type)
	{
		$scope.minuta.secretario = "";
		$scope.minuta.secretario_ext = "";
		if(type == 2)
			$scope.secExt = true
		if(type == 1)
			$scope.secExt = false;
		if(!$scope.$$phase)$scope.$apply();
	}
	
	angular.element(document).ready(function () {
		$scope.iniAdd();
	});
}]);

app.controller('viewMinutasCtrl', ['$scope', '$rootScope','$timeout','$funciones','$location','$service','urlService',function($scope, $rootScope,$timeout,$funciones,$location,$service,urlService){

	$scope.iniView = function()
	{	
		$scope.minuta = {};
		$scope.isEdit = true;
		let parametrosViewMinuta = {idMinuta:$rootScope.idMinutaView};
		$service.POST(urlService.getInfoMinuta,parametrosViewMinuta,$scope.viewMinutaSuccess,$rootScope.serviceError);
	}
	
	$scope.viewMinutaSuccess = function(dataMinuta)
	{
		var minuta = dataMinuta.data;
		$scope.minuta = minuta.data;
		$scope.participantes = minuta.participantes;
		$scope.objetivos = minuta.objetivos;
		$scope.actividades = minuta.actividades;
		if(!$scope.$$phase)$scope.$apply();
	}
	
	angular.element(document).ready(function () {
		$scope.iniView();
	});

}]);
app.controller('editMinutasCtrl', ['$scope', '$rootScope','$timeout','$funciones','$location','$service','urlService','$interval',function($scope, $rootScope,$timeout,$funciones,$location,$service,urlService,$interval){
	
	$scope.iniEdit = function()
	{	
		$scope.minuta = {};
		$scope.isEdit = true;
		$scope.getAsuntos();
		$service.GET(urlService.userActive,null,$scope.userActiveSuccess,$rootScope.serviceError);
	}
	
	$scope.getInfoMinutaSuccess = function(dataMinuta)
	{
		
		$scope.infoMinuta = dataMinuta.data;
		let minutaData = $scope.infoMinuta.data;
		
		$scope.minuta = {
			minutaId:parseInt(minutaData.minuta),
			nombre:minutaData.name,
			organizador:$funciones.objectFindByKey($scope.users,"idUser",minutaData.organizador),
			organizador_ext:minutaData.organizador_ext,
			secretario:$funciones.objectFindByKey($scope.users,"idUser",minutaData.secretario),
			secretario_ext:minutaData.secretario_ext,
			fechaReunion:"",
			versionParent:minutaData.versionParent,
			fecha:$funciones.formatDate(new Date(),1),
			timeInicio:$funciones.formatDate(new Date(),6),
			timeFin:$funciones.formatDate(new Date(),6),
			fw:$funciones.weekNumber(new Date())
		}
		
		$scope.orgExt = ($scope.minuta.organizador_ext)?true:false;
		$scope.secExt = ($scope.minuta.secretario_ext)?true:false;
		
		$scope.minuta.participantes = [];
		$.each($scope.infoMinuta.participantes,function(index,value){
			let objParticipante = $funciones.objectFindByKey($scope.users,"idUser",value.idUser);
			let dataParticipante = {
				user: objParticipante,
				puesto:objParticipante.puesto,
				asistencia:parseInt(value.asistencia),
			};
			$scope.minuta.participantes.push(dataParticipante);
		});
		
		$scope.minuta.objetivos = [];
		$.each($scope.infoMinuta.objetivos,function(index,value){
			let fechaObjetivo = value.date.split("-");
			let dataParticipante = {
				descripcion: value.description,
				fecha:$funciones.formatDate(new Date(fechaObjetivo[0],fechaObjetivo[1]-1,fechaObjetivo[2]),1),
			};
			$scope.minuta.objetivos.push(dataParticipante);
		});
		
		$scope.minuta.actividades = [];
		$.each($scope.infoMinuta.actividades,function(index,value){
			let dateTimeActividad = value.date.split(" ");
			let fechaActividad = dateTimeActividad[0].split("-");
			let timeActividad = dateTimeActividad[1].split(":");
			let dataParticipante = {
				idActividad: value.idActividad,
				responsable: $funciones.objectFindByKey($scope.users,"idUser",value.idUser),
				asunto: $funciones.objectFindByKey($scope.minutasList,"idMinuta",value.idMinutaLast),
				name:value.name,
				descripcion:value.description,
				fecha:$funciones.formatDate(new Date(fechaActividad[0],fechaActividad[1]-1,fechaActividad[2],timeActividad[0],timeActividad[1]),8),
			};
			
			dataParticipante.coresponsable = [];
			$.each(value.coresponsable,function(indexCorresponsal,valueCorresponsal){
				dataParticipante.coresponsable.push($funciones.objectFindByKey($scope.users,"idUser",valueCorresponsal.idUser));
			});
			$scope.minuta.actividades.push(dataParticipante);
		});
		
		// $scope.minuta.actividades = $scope.infoMinuta.actividades;
		if(!$scope.$$phase)$scope.$apply();
		
		$timeout(function(){
			autosize(document.querySelectorAll('textarea'));
		},100);
		
		$interval(function() {
			$scope.minuta.timeFin = $funciones.formatDate(new Date(),6);
			if(!$scope.$$phase)$scope.$apply();
		}, 1000);
	}
	
	$scope.userActiveSuccess = function(dataUsers)
	{
		console.log(dataUsers.data);
		$scope.users = dataUsers.data;
		
		$service.GET(urlService.minutasList,null,$scope.selectAsuntoMinuta,$rootScope.serviceError);
	}
	
	$scope.selectAsuntoMinuta = function(listMinuta)
	{
		$scope.minutasList = listMinuta.data;
		
		if(!$scope.$$phase)$scope.$apply();
		
		let parametrosEditMinuta = {idMinuta:$rootScope.idMinutaEdit};
		$service.POST(urlService.getInfoMinuta,parametrosEditMinuta,$scope.getInfoMinutaSuccess,$rootScope.serviceError);
	}
	
	$scope.selectPuesto = function(index)
	{
		let puesto = "";
		let user = $scope.minuta.participantes[index].user;
		$.each($scope.users,function(index,value){
			if(value.idUser == user.idUser)
			{
				puesto = value.puesto;
				
				return false;
			}
		});
		$scope.minuta.participantes[index].puesto = puesto;
		
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.addParticipante = function()
	{
		let dataParticipante = {user:null,puesto:"",asistencia:false};
		$scope.minuta.participantes.push(dataParticipante);
		$timeout(function(){
			$('.toggle-participantes').bootstrapToggle({
				onstyle:"success",
				offstyle:"danger",
			});
		},100);
	}
	
	$scope.addObjetivo = function()
	{
		let dataObjetivo = {descripcion:"",fecha:$funciones.formatDate(new Date(),1)};
		$scope.minuta.objetivos.push(dataObjetivo);
		$timeout(function(){
			autosize(document.querySelectorAll('textarea'));
		},100);
	}
	
	$scope.addActividad = function()
	{
		let dataActividad = {responsable:"",name:"",asunto:"",descripcion:"",coresponsable:"",fecha:$funciones.formatDate(new Date(),8)};
		$scope.minuta.actividades.push(dataActividad);
		$timeout(function(){
			autosize(document.querySelectorAll('textarea'));
		},100);
	}
	
	$scope.remove = function(array, index){
		array.splice(index, 1);
	}
	
	$scope.getWeekNumer = function()
	{
		let fechaMinuta = $scope.minuta.fecha.split("-");
		console.log(new Date(fechaMinuta[2],fechaMinuta[1],fechaMinuta[0]))
		$scope.minuta.fw = $funciones.weekNumber(new Date(fechaMinuta[2],fechaMinuta[1]-1,fechaMinuta[0]));
	}
	
	$scope.editMinuta = function()
	{
		let parametrosSave = {data:$scope.minuta,idUser:$rootScope.nctechInfo.idUser};
		console.log(parametrosSave);
		$service.POST(urlService.editMinuta,parametrosSave,$scope.editSuccess,$rootScope.serviceError);
	}
	
	$scope.editSuccess = function(dataSave)
	{
		$rootScope .idMinutaEdit = null;
		$location.path("/app/minutas");
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.getAsuntos = function()
	{
		$service.GET(urlService.getAsuntosMinuta,null,$scope.getAsuntosMinutaSuccess,$rootScope.serviceError);
	}
	
	$scope.getAsuntosMinutaSuccess = function(dataAsuntos)
	{
		$scope.asuntos = dataAsuntos.data;
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.addAsunto = function()
	{
		$("#modal-addAsunto").modal("show");
	}
	
	$scope.saveAsunto = function()
	{
		let parametrosSave = {data:$scope.asunto};
		console.log(parametrosSave);
		$service.POST(urlService.saveAsunto,parametrosSave,$scope.saveAsuntoSuccess,$rootScope.serviceError);
	}
	
	$scope.saveAsuntoSuccess = function(dataAsunto)
	{
		new PNotify.success({
			title: 'Upss!',
			text: dataAsunto.msg,
			type: 'success',
			styling:'bootstrap4',
			icons:'fontawesome4',
			hide:true,
			delay:3000,
		});
		$("#modal-addAsunto").modal("hide");
		$scope.getAsuntos();
	}
	
	$scope.setOrgnizador = function(type)
	{
		$scope.minuta.organizador = "";
		$scope.minuta.organizador_ext = "";
		if(type == 2)
			$scope.orgExt = true
		if(type == 1)
			$scope.orgExt = false;
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.setSecretario = function(type)
	{
		$scope.minuta.secretario = "";
		$scope.minuta.secretario_ext = "";
		if(type == 2)
			$scope.secExt = true
		if(type == 1)
			$scope.secExt = false;
		if(!$scope.$$phase)$scope.$apply();
	}
	
	angular.element(document).ready(function () {
		$scope.iniEdit();
	});
}]);