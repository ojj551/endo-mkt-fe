app.controller('ProcedimientosCtrl', ['$scope', '$rootScope','$timeout','$funciones','$location','$service','urlService','$ngConfirm','DTOptionsBuilder','DTColumnDefBuilder',function($scope, $rootScope,$timeout,$funciones,$location,$service,urlService,$ngConfirm,DTOptionsBuilder,DTColumnDefBuilder){
	
	$scope.ini = function()
	{
		$service.GET(urlService.getProcedimientos,null,$scope.getProcedimientosSuccess,$rootScope.serviceError);
	}
	
	$scope.getProcedimientosSuccess = function(dataProcedimientos)
	{
		console.log(dataProcedimientos);
		$scope.procesos = dataProcedimientos.data;
		$timeout(function(){
			$scope.configDatatable();
		},1000);
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.configDatatable = function()
	{
		$scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() { return $q.when($scope.minutas);})
			.withBootstrap()
			.withDisplayLength(10)
			.withOption('order', [[4, 'desc']])
			.withLanguage({
                "sInfo": "Mostrando registros del _START_ al _END_",
				"sProcessing": "Procesando...",
				"sLengthMenu": "Mostrar _MENU_ registros",
				"sZeroRecords": "No se encontraron resultados",
				"sEmptyTable": "Ningún dato disponible en esta tabla",
				"sInfoEmpty": "Mostrando registros del 0 al 0",
				"sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix": "",
				"sSearch": "Buscar:",
				"sUrl": "",
				"sInfoThousands": ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
				"sFirst": "Primero",
				"sLast": "Último",
				"sNext": "Siguiente",
				"sPrevious": "Anterior"
				},
				"oAria": {
				"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
            })
			.withDOM(	"<'col-md-12'<'row'<'col-md-6'l><'col-md-6'f>>>" +
						"t" +
						"<'col-md-12'<'row'<'col-md-5'i><'col-md-7'p>>>"
					);
		$scope.dtColumnDefs = [
			DTColumnDefBuilder.newColumnDef(0),
			DTColumnDefBuilder.newColumnDef(1),
			DTColumnDefBuilder.newColumnDef(2),
			DTColumnDefBuilder.newColumnDef(3),
			DTColumnDefBuilder.newColumnDef(4),
			DTColumnDefBuilder.newColumnDef(5),
			DTColumnDefBuilder.newColumnDef(6).notSortable(),
		];
	}
	
	$scope.editProcedimientos = function(idProceso)
	{
		$rootScope.idProcesoEdit = idProceso;
		$location.path("/app/editProcedimientos");
	}
	
	$scope.viewProceso = function(idProceso,isModal = false)
	{
		if(isModal)
			$("#modal-versionProceso").modal("hide");
		$rootScope.idProcesoView = idProceso;
		$location.path("/app/viewProcedimientos");
	}
	
	$scope.versionProceso = function(idParent,name)
	{
		$scope.nameVersionProceso = name;
		let parametrosProceso = {parent:idParent};
		$service.GET(urlService.getProcesosVersion,parametrosProceso,$scope.getProcesosVersionSuccess,$rootScope.serviceError);
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.getProcesosVersionSuccess = function(dataProcesos)
	{
		console.log(dataProcesos);
		$scope.procesosVersion = dataProcesos.data;
		$("#modal-versionProceso").modal("show");
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.closeVersionProceso = function()
	{
		$scope.procesosVersion = [];
		$scope.nameVersionProceso = null;
		$("#modal-versionProceso").modal("hide");
		if(!$scope.$$phase)$scope.$apply();
	}
	
	angular.element(document).ready(function () {
		$scope.ini();
	});
}]);
app.controller('addProcedimientosCtrl', ['$scope', '$rootScope','$timeout','$funciones','$location','$service','urlService',function($scope, $rootScope,$timeout,$funciones,$location,$service,urlService){
	
	$scope.ini = function()
	{		
		$scope.isEdit = false;
		$scope.proceso = {};
		$scope.proceso.lineamientos = [];
		$scope.proceso.pasos = [];
		$scope.proceso.list = [];
		$scope.proceso.responsables = [];
		
		$scope.addLineamiento();
		
		$scope.addPaso();
		
		$scope.addList();
		
		$scope.addResponsable();
				
		console.log($scope.minuta);
		if(!$scope.$$phase)$scope.$apply();
		
		$service.GET(urlService.userActive,null,$scope.userActiveSuccess,$rootScope.serviceError);
		
		$service.GET(urlService.areas,null,$scope.areasActiveSuccess,$rootScope.serviceError);
		
		$timeout(function(){
			autosize(document.querySelectorAll('textarea'));
		},100);
		
	}
	
	$scope.userActiveSuccess = function(dataUsers)
	{
		console.log(dataUsers.data);
		$scope.users = dataUsers.data;
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.areasActiveSuccess = function(dataAreas)
	{
		console.log(dataAreas.data);
		$scope.areas = dataAreas.data;
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.addLineamiento  = function()
	{
		let dataLineamiento = {name:"",objetivo:""};
		$scope.proceso.lineamientos.push(dataLineamiento);
	}
	
	$scope.addPaso = function()
	{
		let dataPaso = {name:"",area:null};
		$scope.proceso.pasos.push(dataPaso);
	}
	
	$scope.addList = function()
	{
		let dataList = {name:"",type:2};
		$scope.proceso.list.push(dataList);
	}
	
	$scope.addResponsable = function()
	{
		let dataResponsable = {user:null,area:null};
		$scope.proceso.responsables.push(dataResponsable);
	}
	
	$scope.remove = function(array, index){
		array.splice(index, 1);
	}
	
	$scope.saveProceso = function()
	{
		let parametrosSave = {data:$scope.proceso,idUser:$rootScope.nctechInfo.idUser};
		console.log(parametrosSave);
		$service.POST(urlService.saveProceso,parametrosSave,$scope.saveSuccess,$rootScope.serviceError);
	}
	
	$scope.saveSuccess = function(dataSave)
	{
		$location.path("/app/procedimientos");
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.ini();
}]);

app.controller('viewProcedimientosCtrl', ['$scope', '$rootScope','$timeout','$funciones','$location','$service','urlService',function($scope, $rootScope,$timeout,$funciones,$location,$service,urlService){

	$scope.iniView = function()
	{	
		$scope.proceso = {};
		let parametrosViewProceso = {idProceso:$rootScope.idProcesoView};
		$service.POST(urlService.getInfoProceso,parametrosViewProceso,$scope.viewProcesoSuccess,$rootScope.serviceError);
	}
	
	$scope.viewProcesoSuccess = function(dataProceso)
	{
		var proceso = dataProceso.data;
		$scope.proceso = proceso.data;
		$scope.proceso.lineamientos = proceso.lineamientos;
		$scope.proceso.pasos = proceso.pasos;
		$scope.proceso.list = proceso.list;
		$scope.proceso.responsables = proceso.responsables;
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.iniView ();

}]);
app.controller('editProcedimientosCtrl', ['$scope', '$rootScope','$timeout','$funciones','$location','$service','urlService',function($scope, $rootScope,$timeout,$funciones,$location,$service,urlService){
	
	$scope.ini = function()
	{	
		$scope.isEdit = true;
		$service.GET(urlService.userActive,null,$scope.userActiveSuccess,$rootScope.serviceError);
		
		$timeout(function(){
			autosize(document.querySelectorAll('textarea'));
		},100);
	}
	
	$scope.userActiveSuccess = function(dataUsers)
	{
		console.log(dataUsers.data);
		$scope.users = dataUsers.data;
		if(!$scope.$$phase)$scope.$apply();
		
		$service.GET(urlService.areas,null,$scope.areasActiveSuccess,$rootScope.serviceError);
	}
	
	$scope.areasActiveSuccess = function(dataAreas)
	{
		console.log(dataAreas.data);
		$scope.areas = dataAreas.data;
		if(!$scope.$$phase)$scope.$apply();
		console.log($rootScope.idProcesoEdit);
		let parametrosEditProceso = {idProceso:$rootScope.idProcesoEdit};
		$service.POST(urlService.getInfoProceso,parametrosEditProceso,$scope.getInfoProcesoSuccess,$rootScope.serviceError);
	}
	
	$scope.getInfoProcesoSuccess = function(dataProceso)
	{
		
		let proceso = dataProceso.data;
		
		$scope.proceso = proceso.data;
		
		$scope.proceso.area = $funciones.objectFindByKey($scope.areas,"idArea",$scope.proceso.idArea);
		$scope.proceso.user = $funciones.objectFindByKey($scope.users,"idUser",$scope.proceso.idUser);
		
		$scope.proceso.lineamientos = proceso.lineamientos;
		$scope.proceso.pasos = proceso.pasos;
		$scope.proceso.list = proceso.list;
		$scope.proceso.responsables = proceso.responsables;
		
		$.each($scope.proceso.responsables,function(index,value){
			value.area = $funciones.objectFindByKey($scope.areas,"idArea",value.idArea);
			value.user = $funciones.objectFindByKey($scope.users,"idUser",value.idUser);
		});
		$.each($scope.proceso.list,function(index,value){
			value.type = parseInt(value.type);
		});
		if(!$scope.$$phase)$scope.$apply();
		console.log($scope.proceso);
	}
	
	$scope.addLineamiento  = function()
	{
		let dataLineamiento = {name:"",objetivo:""};
		$scope.proceso.lineamientos.push(dataLineamiento);
	}
	
	$scope.addPaso = function()
	{
		let dataPaso = {name:"",area:null};
		$scope.proceso.pasos.push(dataPaso);
	}
	
	$scope.addList = function()
	{
		let dataList = {name:"",type:2};
		$scope.proceso.list.push(dataList);
	}
	
	$scope.addResponsable = function()
	{
		let dataResponsable = {user:null,area:null};
		$scope.proceso.responsables.push(dataResponsable);
	}
	
	$scope.remove = function(array, index){
		array.splice(index, 1);
	}
	
	$scope.getWeekNumber = function()
	{
		let fechaIni = $scope.proceso.dateIni.split("-");
		let fechaFin = $scope.proceso.dateEnd.split("-");
		let diffDays = $funciones.getDiffDays(new Date(fechaIni[2],fechaIni[1]-1,fechaIni[0]),new Date(fechaFin[2],fechaFin[1]-1,fechaFin[0]));
		
		$scope.proceso.numWeek = $funciones.getWeekForDay(diffDays);
	}
	
	$scope.editProceso = function()
	{
		let parametrosSave = {data:$scope.proceso,idUser:$rootScope.nctechInfo.idUser};
		console.log(parametrosSave);
		$service.POST(urlService.editProceso,parametrosSave,$scope.editSuccess,$rootScope.serviceError);
	}
	
	$scope.editSuccess = function(dataSave)
	{
		$rootScope .idMinutaEdit = null;
		$location.path("/app/procedimientos");
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.ini();
}]);