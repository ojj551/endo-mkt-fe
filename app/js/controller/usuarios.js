app.controller('Usuarios', ['$scope', '$rootScope','$timeout','$funciones','$location','$service','urlService','$ngConfirm','DTOptionsBuilder','DTColumnDefBuilder','$sce',function($scope, $rootScope,$timeout,$funciones,$location,$service,urlService,$ngConfirm,DTOptionsBuilder,DTColumnDefBuilder,$sce){
	var table = null;
	
	$scope.ini = function()
	{
		$service.GET(urlService.getUsers,"",$scope.listUsers,$rootScope.serviceError);
	}
	
	$scope.reini = function()
	{
		location.reload();
	}
	
	$scope.listUsers = function(dataUsers)
	{
		console.log(dataUsers);
		$scope.usuarios = dataUsers.data;
		$timeout(function(){
			$scope.configDatatable();
		},1000);
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.configDatatable = function()
	{
		$scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() { return $q.when($scope.usuarios);})
			.withBootstrap()
			.withDisplayLength(10)
			.withOption('order', [[4, 'desc']])
			.withLanguage({
                "sInfo": "Mostrando registros del _START_ al _END_",
				"sProcessing": "Procesando...",
				"sLengthMenu": "Mostrar _MENU_ registros",
				"sZeroRecords": "No se encontraron resultados",
				"sEmptyTable": "Ningún dato disponible en esta tabla",
				"sInfoEmpty": "Mostrando registros del 0 al 0",
				"sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix": "",
				"sSearch": "Buscar:",
				"sUrl": "",
				"sInfoThousands": ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
				"sFirst": "Primero",
				"sLast": "Último",
				"sNext": "Siguiente",
				"sPrevious": "Anterior"
				},
				"oAria": {
				"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
            })
			.withDOM(	"<'col-md-12'<'row'<'col-md-6'l><'col-md-6'f>>>" +
						"t" +
						"<'col-md-12'<'row'<'col-md-5'i><'col-md-7'p>>>"
					);
		$scope.dtColumnDefs = [
			DTColumnDefBuilder.newColumnDef(0),
			DTColumnDefBuilder.newColumnDef(1),
			DTColumnDefBuilder.newColumnDef(2),
			DTColumnDefBuilder.newColumnDef(3),
			DTColumnDefBuilder.newColumnDef(4),
			DTColumnDefBuilder.newColumnDef(5),
			DTColumnDefBuilder.newColumnDef(6),
			DTColumnDefBuilder.newColumnDef(7).notSortable(),
		];
	}
	
	$scope.confirmDelete = function(index)
	{
		var dataUsuario = $scope.usuarios[index];
		$.confirm({
			title: 'Aviso!',
			content: '¿Desear Eliminar al usuario '+dataUsuario.name+' '+dataUsuario.lastName+'?',
			useBootstrap:true,
			animationBounce: 2.5,
			animationSpeed: 1000,
			buttons: {
				cancel:{
					text: 'Cancelar!',
					btnClass:'btn-danger'
				},
				confirm: {
					text: 'Si, Continuar', // With spaces and symbols
					btnClass:'btn-success',
					action: function () {
						$scope.statusUser(index,3);
					}
				}
			}
		});
	}
	
	$scope.deleteUser = function(index)
	{
		if($scope.usuarios[index].children)
		{
			$scope.reasignaUsuario(index);
		}
		else
		{
			table.clear();
			table.destroy();
			$scope.viewTable = false;
			$scope.usuarios.splice(index, 1);
			if (!$scope.$$phase) $scope.$apply();
			$scope.viewTable = true;
			if (!$scope.$$phase) $scope.$apply();
			$timeout($scope.iniDataTable,1000);
		}
	}
	
	$scope.deleteUserChildren = function()
	{
		$scope.reini();
	}
	
	$scope.statusUser = function(index,numStatus)
	{
		let parametrosSave = {status:numStatus,idUser:$scope.usuarios[index].idUser};
		$service.POST(urlService.statusUsuario,parametrosSave,$scope.reini,$rootScope.serviceError);
	}
	
	
	$scope.sendAddUser = function()
	{
		$location.path('/app/add_usuarios');
	}
	
	$scope.sendEditUser = function(index)
	{
		$rootScope.editUsuario = $scope.usuarios[index];
		if (!$scope.$$phase) $scope.$apply();
		$location.path('/app/edit_usuarios');
	}
	
	angular.element(document).ready(function () {
		$scope.ini();
	});
}]);

app.controller('addUsuarios', function($scope, $rootScope,$timeout,$location,$service,urlService,$ngConfirm){
	
	$scope.iniAdd = function()
	{
		$scope.usuario = {};
		$scope.isEdit = false;
		$service.GET(urlService.areas,null,$scope.areasActiveSuccess,$rootScope.serviceError);
		$service.GET(urlService.getRoles,null,$scope.rolesActiveSuccess,$rootScope.serviceError);
	}
	
	$scope.areasActiveSuccess = function(dataAreas)
	{
		console.log(dataAreas);
		$scope.areas = dataAreas.data;
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.rolesActiveSuccess = function(dataRoles)
	{
		console.log(dataRoles);
		$scope.roles = dataRoles.data;
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.sendUserList = function()
	{
		$location.path('/app/usuarios');
	}
	
	$scope.saveUsuario = function()
	{
		let parametrosSave = {data:$scope.usuario,idUser:$rootScope.nctechInfo.idUser};
		console.log(parametrosSave);
		$service.POST(urlService.saveUsuario,parametrosSave,$scope.saveSuccess,$rootScope.serviceError);
	}
	
	$scope.saveSuccess = function(dataSave)
	{
		$location.path("/app/usuarios");
		if(!$scope.$$phase)$scope.$apply();
	}
	
	angular.element(document).ready(function () {
		$scope.iniAdd();
	});
});

app.controller('editUsuarios', function($scope, $rootScope,$timeout,$location,$service,urlService,$ngConfirm,$funciones){
	
	$scope.iniEdit = function()
	{
		if(!$rootScope.editUsuario)
		{
			$scope.sendUserList();
			return;
		}

		console.log($rootScope.editUsuario);
		$scope.usuario = $rootScope.editUsuario;
		$scope.usuario.lastname = $scope.usuario.lastName;
		$scope.isEdit = true;
		
		$service.GET(urlService.areas,null,$scope.areasActiveSuccess,$rootScope.serviceError);
		$service.GET(urlService.getRoles,null,$scope.rolesActiveSuccess,$rootScope.serviceError);
		
		if (!$scope.$$phase) $scope.$apply();
	}
	
	$scope.areasActiveSuccess = function(dataAreas)
	{
		console.log(dataAreas);
		$scope.areas = dataAreas.data;
		$scope.usuario.area = $funciones.objectFindByKey($scope.areas,"idArea",$scope.usuario.idArea);
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.rolesActiveSuccess = function(dataRoles)
	{
		console.log(dataRoles);
		$scope.roles = dataRoles.data;
		$scope.usuario.rol = $funciones.objectFindByKey($scope.roles,"idRol",$scope.usuario.idRol);
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.sendUserList = function()
	{
		$location.path('/app/usuarios');
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.editUsuario = function()
	{
		let parametrosSave = {data:$scope.usuario,idUser:$scope.usuario.idUser};
		console.log(parametrosSave);
		$service.POST(urlService.editUsuario,parametrosSave,$scope.sendUserList,$rootScope.serviceError);
	}
	
	angular.element(document).ready(function () {
		$scope.iniEdit();
	});
});