app.controller('ForgetPassCtrl', ['$scope', '$rootScope','$timeout','$sce','$location','$compile','$sce','$service','$ngConfirm','urlService',function($scope, $rootScope,$timeout,$sce,$location,$compile,$sce,$service,$ngConfirm,urlService){
	
	$scope.ini = function()
	{
		$scope.forget = {};
		$scope.getParam = $location.$$search;
		
		if($scope.getParam.token)
		{
			var dataToken = {token:$scope.getParam.token};
		
			$service.POST(urlService.checkTokenPass,dataToken,$scope.checkTokenPassSuccess,$rootScope.checkTokenPassError);
		}
		else
		{
			$scope.viewChangePassword = false;
			$scope.messageError = "Página No Valida";
		}
		
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.checkTokenPassSuccess = function(dataForget)
	{
		if(dataForget.data == 1)
		{
			$scope.viewChangePassword = true;
		}
		else
		{
			$scope.messageError="Este link ha expirado";
		}
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$rootScope.checkTokenPassError = function(dataForget)
	{
		$scope.viewChangePassword = false;
		$scope.messageError =  "TOKEN EXPIRADO";
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.sendForgetPassword = function()
	{
		if($scope.forget.passNew == $scope.forget.passConfirm)
		{
			var parametrosChangePass= {token:$scope.getParam.token,pass:$scope.forget.passNew};
			$service.POST(urlService.changePassForget,parametrosChangePass,$scope.changePassSuccess,$scope.changePassError);
		}
		else
		{
			new PNotify.error({
			title: 'Upss!',
			text: "Las constraseñas no son iguales",
			hide:true,
			delay:3000,
		});
		}
	}
	
	$scope.changePassSuccess = function(dataSuccess)
	{
		$location.path('/app/home');
		if(!$scope.$$phase)$scope.$apply();
		
		new PNotify.success({
			title: 'Exitoso!',
			text: dataSuccess.msg,
			hide:true,
			delay:3000,
		});
	}
	
	$scope.changePassError = function(dataError)
	{
		new PNotify.error({
			title: 'Upss!',
			text: dataError.msg,
			hide:true,
			delay:3000,
		});
	}
	
	angular.element(document).ready(function () {
		$scope.ini();
	});
}]);