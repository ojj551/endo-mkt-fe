app.controller('Notificaciones', function($scope, $rootScope,$timeout,$location,$service,urlService,$funciones,DTOptionsBuilder,DTColumnDefBuilder){
	
	$scope.ini = function()
	{
		$rootScope.notificaciones = [];
		
		var paramView = {idUser:$rootScope.nctechInfo.idUser};
		
		$service.GET(urlService.viewNotification,paramView,$scope.viewNotificationSuccess,$rootScope.serviceError);
	}
	
	$scope.viewNotificationSuccess = function()
	{
		$scope.firstDayWeek = $funciones.getFirstDayWeek();
		$scope.firstLastWeek = $funciones.getLastDayWeek();
		
		$scope.rangeDate = $scope.firstDayWeek+" - "+$scope.firstLastWeek;
		
		var dataRange = {initDate:$scope.firstDayWeek,endDate:$scope.firstLastWeek,idUser:$rootScope.nctechInfo.idUser};
		
		$service.POST(urlService.notificationActive,dataRange,$scope.notificationActiveSuccess,$rootScope.serviceError);
	}
	
	$scope.notificationActiveSuccess = function(listNotify)
	{
		$scope.notifyActives = listNotify.data;
		$timeout(function(){
			$scope.configDatatable();
		},1000);
		if (!$scope.$$phase) $scope.$apply();
	}
	
	$scope.configDatatable = function()
	{
		$scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() { return $q.when($scope.minutas);})
			.withBootstrap()
			.withLanguageSource('//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json')
			.withDisplayLength(10)
			.withOption('order', [[0, 'desc']])
			.withLanguage({
                "sInfo": "Mostrando registros del _START_ al _END_",
            })
			.withDOM(	"<'col-md-12'<'row'<'col-md-6'l><'col-md-6'f>>>" +
						"t" +
						"<'col-md-12'<'row'<'col-md-5'i><'col-md-7'p>>>"
					);
		$scope.dtColumnDefs = [
			DTColumnDefBuilder.newColumnDef(0),
			DTColumnDefBuilder.newColumnDef(1),
			DTColumnDefBuilder.newColumnDef(2),
			DTColumnDefBuilder.newColumnDef(3),
			DTColumnDefBuilder.newColumnDef(4),
			DTColumnDefBuilder.newColumnDef(5).notSortable(),
		];
	}
	
	angular.element(document).ready(function () {
		$scope.ini();
	});
});