app.controller('LoginCtrl', ['$scope', '$rootScope','$timeout','$sce','$location','$compile','$sce','$service','$ngConfirm','urlService',function($scope, $rootScope,$timeout,$sce,$location,$compile,$sce,$service,$ngConfirm,urlService){
	var optionsErrorToast = null;
	
	$scope.ini = function()
	{
		var hours = 24;
		var now = new Date().getTime();
		var setupTime = localStorage.getItem('setupTime');
		
		PNotify.defaults.styling = 'bootstrap4';
		PNotify.defaults.icons = 'fontawesome4';
		
		if (setupTime == null) {
			localStorage.clear();
		} 
		else if(setupTime != "all")
		{
			if(now-setupTime > hours*60*60*1000) {
				localStorage.clear();
			}
		}
		
		let nctechInfo = localStorage.getItem("nctechInfo");

		if(nctechInfo){
			$location.path('/app/home');
		}
		
		$rootScope.showFooter = false;
		$rootScope.showHeader = false;
	}
	
	$scope.iniciarSesion = function()
	{
		if($("#login").valid())
		{
			var parametrosLogue = {user:$scope.user,pass:$scope.pass};
			$service.POST(urlService.auth,parametrosLogue,$scope.logueoSuccess,$scope.logueoError);
			// $service.GET(urlService.auth,"",$scope.logueoSuccess,$scope.logueoError);
		}
	}
	
	$scope.logueoSuccess = function(dataLoguin)
	{
		let dataUser = JSON.stringify(dataLoguin.data);
		localStorage.setItem("nctechInfo", dataUser);
		
		let parametros = {idRol:JSON.parse(dataUser).idRol};
		$service.GET(urlService.rolFunction,parametros,$scope.listFunction,$rootScope.serviceError);
		
		if($scope.recordar){
			localStorage.setItem('setupTime', "all");
		}
		else{
			let now = new Date().getTime();
			localStorage.setItem('setupTime', now);
		}
		
		
		$location.path('/app/home');
		if(!$scope.$$phase)$scope.$apply();
	}
	
	$scope.listFunction = function(dataFunction)
	{
		$rootScope.permisosSystem = dataFunction.data;
		localStorage.setItem('permisos',JSON.stringify(dataFunction.data));
	}
	
	$scope.logueoError = function(dataError)
	{
		new PNotify.error({
			title: 'Upss!',
			text: dataError.msg,
			hide:true,
			delay:3000,
		});
		
		$("#modal-recuperarPassword").modal("hide");
	}
	
	$scope.recuperarPassword = function()
	{
		$scope.correoForgetPass = "";
		$("#modal-recuperarPassword").modal("show");
	}
	
	$scope.sendForgetPassword = function()
	{
		var parametrosForgetPass= {correo:$scope.correoForgetPass};
		$service.POST(urlService.getTokenForget,parametrosForgetPass,$scope.forgerPassSuccess,$scope.logueoError);
	}
	
	$scope.forgerPassSuccess = function(dataSuccess)
	{
		new PNotify.success({
			title: 'Exitoso!',
			text: dataSuccess.msg,
			hide:true,
			delay:3000,
		});
		$("#modal-recuperarPassword").modal("hide");
	}
	
	angular.element(document).ready(function () {
		$scope.ini();
	});
}]);